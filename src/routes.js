import List from './components/List'
import ListPeople from './components/ListPeople'

/*
	array of all global routes of the proyect, in  here we asociated all the components with his routes.
*/
const routes = [
  { path: '/', component: List, name: 'List' },
];

export default routes
