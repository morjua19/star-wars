import mainService from './main.service'

const starWarsService = {
  listsPeople,
  listsPeopleFiltered,
  getPeople,
  getSpecie,
  getHomeWorld,
  listsPlanets
};


export function listsPeople () {
  return mainService.get('/people').then((res) => {
    return res.data
  }).catch((error) => {
    console.error(error);
    return { error: error }
  })
}

export function listsPeopleFiltered (page) {
  return mainService.get(`/people?page=${page}`).then((res) => {
    return res.data
  }).catch((error) => {
    console.error(error);
    return { error: error }
  })
}

export function getPeople (id) {
  return mainService.get(`/people/${id}`).then((res) => {
    return res.data
  }).catch((error) => {
    console.error(error);
    return { error: error }
  })
}

export function getSpecie (id) {
  return mainService.get(`/species/${id}`).then((res) => {
    return res.data
  }).catch((error) => {
    console.error(error);
    return { error: error }
  })
}

export function getHomeWorld (id) {
  return mainService.get(`/planets/${id}`).then((res) => {
    return res.data
  }).catch((error) => {
    console.error(error);
    return { error: error }
  })
}


export function listsPlanets () {
  return mainService.get('/planets').then((res) => {
    return res.data
  }).catch((error) => {
    console.error(error);
    return { error: error }
  })
}

export default starWarsService
